import React from "react";
import './Laps.css'
import {useSelector} from 'react-redux'
function Laps() {
  const LapArray = useSelector(state => state.timer.lapTime)
 
  return (
    <div className="laps">
      {LapArray.map((time)=>
           <p className="lap_record" key={time.key}>{time.timelap}</p>
      )}
     
    </div>
  );
}

export default Laps;
