import React from 'react'
import './Buttons.css'
import { timerActions } from '../store/store'
import {useDispatch,useSelector} from 'react-redux'
function Buttons() {
    const dispatch=useDispatch()
    const isPaused = useSelector(state => state.timer.isPaused)
    const StartHandler=()=>{
        dispatch(timerActions.handleStart())
    }
    const pauseHandler=()=>{
        dispatch(timerActions.handlePauseResume())
    }
    const handleReset=()=>{
        dispatch(timerActions.handleReset())
    }
    const lapHandler=()=>{
        dispatch(timerActions.setLap())
    }
   
    return (
        <div className="buttons">
                <button onClick={StartHandler}  >Start</button>
                <button onClick={pauseHandler}>{isPaused ? "Resume" : "Pause"}</button>
                <button onClick={handleReset} >Reset</button>
                <button onClick={lapHandler} > Laps</button>
        </div>
    )
}

export default Buttons
