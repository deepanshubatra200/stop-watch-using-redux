import React,{useEffect} from 'react'
import './App.css';
import {useSelector,useDispatch} from 'react-redux'
import TimerDisplay from './TimerDisplay/TimerDisplay';
import Buttons from './Buttons/Buttons';
import { timerActions } from './store/store';
import Laps from './Laps/Laps'
function App() {
  const isActive = useSelector(state => state.timer.isActive)
  const isPaused=useSelector(state=> state.timer.isPaused)

  const dispatch=useDispatch()
  useEffect(() => {
    let interval = null;
    
    if (isActive && isPaused === false) {
      interval = setInterval(() => {
        dispatch(timerActions.setTimer(10))
      }, 10);
    } else {
      clearInterval(interval);
    }
    return () => {
      clearInterval(interval);
    };
  }, [isActive, isPaused,dispatch]);




  return (
    <div className="container">
        <TimerDisplay  />
        <Buttons  />
        <Laps />
    </div>
  );
}

export default App;
