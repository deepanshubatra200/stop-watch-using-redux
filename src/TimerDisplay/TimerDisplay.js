import React,{useRef} from 'react'
import './TimerDisplay.css'
import { useSelector,useDispatch } from 'react-redux';
import { timerActions } from '../store/store';
function TimerDisplay() {
    const time = useSelector(state => state.timer.time)
    const dispatch=useDispatch()
    const timeRef=useRef();
    dispatch(timerActions.setCurrentTime(timeRef?.current?.innerText))
    return (
        <div>
             <div className="timerDisplay">
                <p ref={timeRef}>{("0" + Math.floor((time / 360000) % 60)).slice(-2)} : {("0" + Math.floor((time / 60000) % 60)).slice(-2)} : {("0" + Math.floor((time / 1000) % 60)).slice(-2)} : {("0" + ((time / 10) % 100)).slice(-2)}</p>

            </div>
        </div>
    )
}

export default TimerDisplay












                